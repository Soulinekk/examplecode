﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class Extensions
{
    #region consts
    public const string scriptableObjectsResourcePath = "ScriptableObjects/";
    public const string gizmoDrawerGameObjectName = "[Gizmo Drawer Controller]";
    #endregion
}