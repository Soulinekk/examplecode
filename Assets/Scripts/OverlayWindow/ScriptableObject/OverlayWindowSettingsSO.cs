﻿namespace OverlayWindow
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using UnityEngine;

    [CreateAssetMenu(fileName = "OverlayWindowSettings", menuName = "ScriptableObjects/Create OverlayWindowSettings", order = 1)]
    public class OverlayWindowSettingsSO : ScriptableObject
    {
        public string GUISkinPath;
        public float windowWidth = 300;
        public float windowHeight = 300;
        public Vector2 foldoutButtonSize = new Vector2(20f, 20f);
    }
}