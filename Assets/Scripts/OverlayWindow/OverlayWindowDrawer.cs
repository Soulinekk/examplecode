﻿#if UNITY_EDITOR

// Comment line below to test how it works outside of editor. 
#define EDITOR

#endif

namespace OverlayWindow
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    using System.Linq;
    using GizmosDrawer;

    public static class OverlayWindowDrawer
    {
#if EDITOR
        public static OverlayWindowSettingsSO settingsData;
        

        #region GUI variables
        public static GUISkin skin;
        private static GUISkin originalSkin;

        private static bool isWindowFolded = false;
        private static int selectedTabIndex = 0;
        private static string[] tabsNames =
        {
            "GIZMO DRAWER",
            "EMPTY TAB",
            "EMPTIER TAB"
        };
        private static GUIStyle tabsStyle;
        private static GUIStyle _foldoutButtonStyle;
        private static GUIStyle FoldoutButtonStyle
        {
            get
            {
                if(_foldoutButtonStyle != null)
                {
                    return _foldoutButtonStyle;
                }

                return GUI.skin.button;
            }
        }

        private static GUIStyle _buttonLightStyle;
        private static GUIStyle ButtonLightStyle
        {
            get
            {
                if (_buttonLightStyle != null)
                {
                    return _buttonLightStyle;
                }

                return FoldoutButtonStyle;
            }
        }
        #endregion


        private static bool _isEnabled;
        public static bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                if (value)
                {
                    if (!TryInitialize())
                    {
                        Hide();
                        return;
                    }
                }
                else { Hide(); }

                _isEnabled = value;
            }
        }

        #region GizmoDrawer
        public static GizmoDrawerController gizmoDrawer;

        public static string gizmoDrawerToggleText
        {
            get
            {
                if(gizmoDrawer != null && gizmoDrawer.IsEnabled)
                {
                    return "Disable";
                }

                return "Enable";
            }
        }

        private static ExampleObjectsSpawner _exampleSpawner;
        public static ExampleObjectsSpawner ExampleSpawner
        {
            get
            {
                if(_exampleSpawner == null)
                {
                    _exampleSpawner = GameObject.FindObjectOfType<ExampleObjectsSpawner>();
                    if(_exampleSpawner == null && gizmoDrawer != null)
                    {
                        _exampleSpawner = gizmoDrawer.gameObject.AddComponent<ExampleObjectsSpawner>();
                    }
                }

                return _exampleSpawner;
            }
        }
        #endregion

        [MenuItem("Overlay Window/Toggle overlay window", false, 1000)]
        public static void ToggleWindow()
        {
            IsEnabled = !IsEnabled;
        }

        /// <summary>
        /// Initializing whole behaviour, and returns <i><color=blue>true</color></i> if initialized successfully.
        /// </summary>
        public static bool TryInitialize()
        {
            var tempSOArray = Resources.LoadAll<OverlayWindowSettingsSO>(Extensions.scriptableObjectsResourcePath);
            
            if (tempSOArray == null || tempSOArray.Length < 1)
            {
                Debug.LogError
                ("<color=cyan>ABANDONING! : </color>Couldn't find scriptable object with window settings. Try creating (Right click in project window -> ScriptableObjects -> Create OverlayWindowSettings)");
                return false;
            }
            settingsData = tempSOArray[0];

            if(!string.IsNullOrEmpty(settingsData.GUISkinPath))
            {
                skin = (GUISkin)Resources.Load(settingsData.GUISkinPath);
                tabsStyle = (GUIStyle)skin.customStyles.FirstOrDefault(x => x.name == "TabView");
                _foldoutButtonStyle = (GUIStyle)skin.customStyles.FirstOrDefault(x => x.name == "FoldoutButton");
                _buttonLightStyle = (GUIStyle)skin.customStyles.FirstOrDefault(x => x.name == "FoldoutButtonLight");
            }
            
            SceneView.onSceneGUIDelegate += Draw;

            return true;
        }

        public static void Hide()
        {
            SceneView.onSceneGUIDelegate -= Draw;
        }

        [MenuItem("Overlay Window/Deinitialize overlay window", false, 1001)]
        public static void Deinitialize()
        {
            ToggleWindow();
            DeinitializeGizmoDrawer();
        }

        /// <summary>
        /// Draws overlay window on scenery window.
        /// </summary>
        public static void Draw(SceneView sceneView)
        {
            UseCustomSkin(true);
            // Allows drawing overlay in scene window, instead of world space (kindish).
            Handles.BeginGUI();
            {
                EditorGUILayout.BeginVertical("box", GUILayout.MaxWidth(settingsData.windowWidth));
                {
                    WindowContent();
                }
                EditorGUILayout.EndVertical();
            }
            Handles.EndGUI();
            UseCustomSkin(false);
        }
        
        // Window content, like buttons, toggles etc. 
        public static void WindowContent()
        {
            EditorGUILayout.BeginHorizontal();
            {
                if(tabsStyle != null)
                {
                    selectedTabIndex = GUILayout.Toolbar(selectedTabIndex, tabsNames, tabsStyle);
                }
                else
                {
                    selectedTabIndex = GUILayout.Toolbar(selectedTabIndex, tabsNames);
                }

                GUILayout.Space(5f);

                if (GUILayout.Button("", FoldoutButtonStyle, GUILayout.Height(18f), GUILayout.Width(18f)))
                {
                    isWindowFolded = !isWindowFolded;
                }
            }
            EditorGUILayout.EndHorizontal();
            if(!isWindowFolded)
            {
                switch (selectedTabIndex)
                {
                    case 0:

                        GizmoDrawerWindow();

                        break;

                    default:

                        EditorGUILayout.BeginVertical("box");
                        {
                            GUILayout.Label("Can you feel the nothingness?");
                        }
                        EditorGUILayout.EndVertical();
                        break;
                }
            }
            
        }

        #region GizmoDrawer
        private static void GizmoDrawerWindow()
        {
            Color originalColor;

            UseCustomSkin(true);
            EditorGUILayout.BeginVertical("box");
            {
                if (gizmoDrawer != null)
                {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("Example Objects:");
                        GUILayout.Space(20f);
                        GUILayout.Label("Current amount:");

                        int alfableObjCount = GizmoDrawerController.alfableObjects != null ? GizmoDrawerController.alfableObjects.Count : 0;
                        GUILayout.Label(alfableObjCount.ToString(), "box", GUILayout.Width(30f));
                    }
                    GUILayout.EndHorizontal();

                    GUILayout.BeginHorizontal();
                    {
                        if (GUILayout.Button("Spawn"))
                        {
                            ExampleSpawner.Spawn();
                        }
                        if (GUILayout.Button("Despawn"))
                        {
                            ExampleSpawner.Despawn();
                        }
                    }
                    GUILayout.EndHorizontal();

                    originalColor = GUI.backgroundColor;
                    GUI.backgroundColor = Color.red;
                    if (GUILayout.Button("Deinitialize", ButtonLightStyle, GUILayout.MaxHeight(25)))
                    {
                        DeinitializeGizmoDrawer();
                    }
                    GUI.backgroundColor = originalColor;
                }
                else
                {
                    originalColor = GUI.backgroundColor;
                    GUI.backgroundColor = Color.green;
                    if (GUILayout.Button("Initialize", ButtonLightStyle, GUILayout.MaxHeight(50)))
                    {
                        InitializeGizmoDrawer();
                    }
                    GUI.backgroundColor = originalColor;
                }
            }
            EditorGUILayout.EndVertical();

            UseCustomSkin(false);
        }

        public static void InitializeGizmoDrawer()
        {
            GetOrCreateGizmoDrawerController();
        }

        public static void DeinitializeGizmoDrawer()
        {
            GameObject.DestroyImmediate(gizmoDrawer.gameObject);
            if(ExampleSpawner != null)
            {
                GameObject.DestroyImmediate(ExampleSpawner.gameObject);
            }
        }

        public static GizmoDrawerController TryGetGizmoDrawerController()
        {
            return GameObject.FindObjectOfType<GizmoDrawerController>();
        }

        private static void GetOrCreateGizmoDrawerController()
        {
            gizmoDrawer = TryGetGizmoDrawerController();

            if(gizmoDrawer == null)
            {
                GameObject go = new GameObject();
                go.name = Extensions.gizmoDrawerGameObjectName;
                gizmoDrawer = go.AddComponent<GizmoDrawerController>();
            }
        }
        #endregion


        private static void UseCustomSkin(bool value)
        {
            if (skin == null) return;

            if (value)
            {
                originalSkin = GUI.skin;
                GUI.skin = skin;
            }
            else
            {
                GUI.skin = originalSkin;
            }
        }
#endif
    }
}