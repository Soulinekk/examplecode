﻿namespace GizmosDrawer
{
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "GizmoDrawerSettings", menuName = "ScriptableObjects/Create GizmoDrawerSettings", order = 2)]
    public class GizmoDrawerSettingsSO : ScriptableObject
    {
        public List<GizmoMesh> gizmoTypes;
    }

    [System.Serializable]
    public class GizmoMesh
    {
        public GizmoTypeCustom gizmoType;
        public Mesh mesh;
    }
}