﻿namespace GizmosDrawer
{
    using System.Collections.Generic;
    using UnityEngine;

    [CreateAssetMenu(fileName = "GizmoObjectsType", menuName = "ScriptableObjects/Create GizmoObjectsTypes", order = 2)]
    public class GizmoObjectsTypesSO : ScriptableObject
    {
        [SerializeField]
        public List<GizmoObjectData> types = new List<GizmoObjectData>();
    }
}