﻿namespace GizmosDrawer
{
    using System;
    using UnityEngine;

    [System.Serializable]
    public class GizmoObjectData
    {
        public string name;
        public Vector3 gizmoSize = Vector3.one;
        public Color gizmoColor = Color.white;
        public GizmoType gizmoType = GizmoType.Box;
        public GameObject spawnBehaviour;

        #region Constructors
        public GizmoObjectData(string name, Vector3 gizmoSize, Color gizmoColor, GizmoType gizmoType)
        {
            this.name = name;
            this.gizmoSize = gizmoSize;
            this.gizmoColor = gizmoColor;
            this.gizmoType = gizmoType;
        }

        public GizmoObjectData(string name, Vector3 gizmoSize, Color gizmoColor, GizmoType gizmoType, GameObject spawnBehaviour)
        {
            this.name = name;
            this.gizmoSize = gizmoSize;
            this.gizmoColor = gizmoColor;
            this.gizmoType = gizmoType;
            this.spawnBehaviour = spawnBehaviour ?? throw new ArgumentNullException(nameof(spawnBehaviour));
        }
        #endregion
    }
}