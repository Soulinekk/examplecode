﻿namespace GizmosDrawer
{
    // Keep value's int index same to GizmoTypeCustom!
    public enum GizmoType
    {
        Box = 0,
        Sphere = 1,
        Crystal = 2,
        Arrow = 3
    }

    // Keep value's int index same to GizmoType!
    public enum GizmoTypeCustom
    {
        Crystal = 2,
        Arrow = 3
    }
}