﻿using System.Collections.Generic;
using UnityEngine;
using Spawner;
using GizmosDrawer;
using System.Linq;

public class ExampleObjectsSpawner : MonoBehaviour
{
    private readonly string parentName = "[Gizmo Objects Container]";

    public Vector3 startPosition = Vector3.zero;
    public uint quantityToSpawn = 10;
    public SpawnBounds spawnBounds = new SpawnBounds(50, 2, 50);
    public SpawnMode spawnMode = SpawnMode.Random;
    public bool avoidOverlaping = true;
    public List<IObjectSpawner> spawners;

    private GizmoObjectsTypesSO _gizmoSettings;
    private GizmoObjectsTypesSO GizmoSettings
    {
        get
        {
            if(_gizmoSettings == null)
            {
                var cachedAssets = Resources.LoadAll<GizmoObjectsTypesSO>(Extensions.scriptableObjectsResourcePath);
                if (cachedAssets != null)
                {
                    _gizmoSettings = cachedAssets[0];
                }
                else
                {
                    Debug.Log($"{Extensions.scriptableObjectsResourcePath} doesnt contains GizmoObjectsTypesSO scriptable object asset.");
                    return null;
                }
            }

            return _gizmoSettings;
        }
    }

    private Transform _objectsParent;
    private Transform ObjectsParent
    {
        get
        {
            if(_objectsParent == null)
            {
                GameObject go = GameObject.Find(parentName);
                if(go == null)
                {
                    go = new GameObject();
                    go.name = parentName;
                    _objectsParent = go.transform;
                }
                else
                {
                    _objectsParent = go.transform;
                }
            }

            return _objectsParent;
        }
    }
    
    public void Spawn()
    {
        spawners = GizmoSettings.types.Select(x => x.spawnBehaviour.GetComponent<IObjectSpawner>()).ToList();

        if ((spawnBounds.x * spawnBounds.y * spawnBounds.z) < 1)
        {
            Debug.Log("Object spawn abandoned, incorrect spawn quantity.");

            return;
        }

        SpawnBounds spawnPosition;
        List<SpawnBounds> UsedSpawnPositions = new List<SpawnBounds>();
        Vector3 v3Position;

        for (int i = 0; i < quantityToSpawn; i++)
        {
            int secureLock = 0;
            do
            {
                spawnPosition = new SpawnBounds(Random.Range(0, spawnBounds.x), Random.Range(0, spawnBounds.y), Random.Range(0, spawnBounds.y));
                secureLock++;
                if (secureLock > 500)
                {
                    spawnPosition = new SpawnBounds(0, 0, 0);
                    break;
                }
            } while (UsedSpawnPositions.Contains(spawnPosition));
            UsedSpawnPositions.Add(spawnPosition);
            v3Position = new Vector3(spawnPosition.x, spawnPosition.y, spawnPosition.z);

            int randomizedIndex = Random.Range(0, GizmoSettings.types.Count);
            spawners[randomizedIndex].Spawn(ObjectsParent, v3Position, GizmoSettings.types[randomizedIndex]);
        }
    }

    public void Despawn()
    {
        if (_objectsParent == null) return;

        DestroyImmediate(ObjectsParent.gameObject);
    }

    public void SetSettings(Vector3 startPosition, uint quantity, int xSpawnRange, int ySpawnRange, int zSpawnRange, SpawnMode mode, bool avoidOverlaping)
    {
        this.startPosition = startPosition;
        quantityToSpawn = quantity;
        spawnBounds.x = xSpawnRange;
        spawnBounds.y = ySpawnRange;
        spawnBounds.z = zSpawnRange;
        this.spawnMode = mode;
        this.avoidOverlaping = avoidOverlaping;
    }
}

/// <summary>
/// Has to be greater than 0!
/// </summary>
public struct SpawnBounds
{
    public int x;
    public int y;
    public int z;

    public SpawnBounds(int x, int y, int z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

/// <summary>
/// Determines how consistently objects are placed to each other.
/// </summary>
public enum SpawnMode
{
    Consistent = 0, 
    Random = 1
}
