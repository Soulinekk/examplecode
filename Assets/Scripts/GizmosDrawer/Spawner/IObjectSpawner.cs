﻿namespace Spawner
{
    using UnityEngine;
    using GizmosDrawer;

    // I know, it could be done better, but have mercy I have other things to do too :D 
    public interface IObjectSpawner
    {
        void Spawn(Transform parent, Vector3 positionOrigin, GizmoObjectData settings);
        void InitializeObject(GizmoObjectData settings);
    }
}