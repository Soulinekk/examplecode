﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spawner;
using GizmosDrawer;

public class PickupSpawner : MonoBehaviour, IObjectSpawner
{
    public int childrenAmount = 4;

    private GameObject groupGO;
    PickupGroupGizmo pickupScript;
    private GameObject childGO;
    private PickupSpawnGizmo[] children;

    /// <summary>
    /// Spawns Pickup parent group and its children beneth it.
    /// </summary>
    public void Spawn(Transform parent, Vector3 positionOrigin, GizmoObjectData settings)
    {
        groupGO = new GameObject("PICKUP GROUP");
        groupGO.transform.SetParent(parent);
        groupGO.transform.localPosition = positionOrigin;
        pickupScript = groupGO.AddComponent<PickupGroupGizmo>();
        children = new PickupSpawnGizmo[childrenAmount];

        Vector3 possibleSpawn;
        List<Vector3> usedSpawnAreas = new List<Vector3>();
        for (int i = 0; i < childrenAmount; i++)
        {
            int securityBreak = 0; 
            do
            {
                possibleSpawn = new Vector3(Random.Range(-childrenAmount, childrenAmount), Random.Range(0, childrenAmount), Random.Range(-childrenAmount, childrenAmount));

                securityBreak++;
                if(securityBreak > 100)
                {
                    Debug.Log($"Ops something went wrong while randomizing possible spawn for {this.name} Spawn(). Picking constanat value");
                    possibleSpawn = Vector3.one;
                    break; 
                }

            } while (usedSpawnAreas.Contains(possibleSpawn));
            usedSpawnAreas.Add(possibleSpawn);

            childGO = new GameObject("Pickup position");
            childGO.transform.SetParent(groupGO.transform);
            childGO.transform.localPosition = possibleSpawn;
            children[i] = childGO.AddComponent<PickupSpawnGizmo>();
            
        }

        InitializeObject(settings);

        Debug.Log("Example Pickups spawned");
    }

    public void InitializeObject(GizmoObjectData settings)
    {
        pickupScript.SetSettings(new GizmoObjectData("PICKUP GROUP", Vector3.one, Color.cyan, GizmoType.Sphere));
        for (int i = 0; i < children.Length; i++)
        {
            children[i].SetSettings(settings);
        }

        pickupScript.children = new List<IGizmoDrawer>();
    }
}