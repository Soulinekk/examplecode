﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GizmosDrawer;

[ExecuteInEditMode]
public class ObstacleGroupGizmo : GizmoDrawer
{
    private bool drawLines = false;

    private void Start()
    {
        GizmoDisplayedName = "ObstacleSpawnGroup";
    }

    private void OnDrawGizmos()
    {
        DrawMeshGizmo(GizmoDrawerController.CustomMeshContainer[GizmoType.Sphere], GizmoSize, GizmoColor);
        Gizmos.DrawIcon(Vector3.zero, GizmoDisplayedName);

        if (drawLines)
        {
            foreach (PlayerSpawnGizmo child in children)
            {
                Gizmos.DrawLine(child.transform.position, transform.position);
            }
        }
    }

    public override void HandleChildrenAlfaChange(AlfaState alfaState)
    {
        foreach (PlayerSpawnGizmo child in children)
        {
            child.HandleObjectAlfaChange(alfaState);
        }
    }

    public override void HandleObjectAlfaChange(AlfaState alfaState)
    {
        if (alfaState == AlfaState.selected)
        {
            drawLines = true;
            CurrentAlfa = SelectedObjectAlfa;
        }
        else
        {
            drawLines = false;
            CurrentAlfa = DefaultObjectsAlfa;
        }
    }
}
