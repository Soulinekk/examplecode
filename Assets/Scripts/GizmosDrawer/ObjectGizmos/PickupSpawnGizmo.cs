﻿using GizmosDrawer;
using UnityEngine;

[ExecuteInEditMode]
public class PickupSpawnGizmo : GizmoDrawer
{
    public PickupGroupGizmo parent;

    private void Start()
    {
        parent = GetComponentInParent<PickupGroupGizmo>();
        if(parent != null && !parent.children.Contains(this))
        {
            parent.children.Add(this);
        }
    }

    private void OnDrawGizmos()
    {
        DrawGizmos();
    }

    public override void HandleChildrenAlfaChange(AlfaState alfaState)
    {
        // Those guys are infertile, good for them [*]
    }

    public override void HandleObjectAlfaChange(AlfaState alfaState)
    {
        CurrentAlfa = (alfaState == AlfaState.selected) ? SelectedObjectAlfa : DefaultObjectsAlfa;
    }

    private void OnDestroy()
    {
        if(parent != null)
        {
            parent.children.Remove(this);
        }
    }
}
