﻿#if UNITY_EDITOR
    #define EDITOR
#endif

using GizmosDrawer;
using System.Collections.Generic;
using UnityEngine;

#if EDITOR
    using UnityEditor;
#endif

[ExecuteInEditMode]
public class PickupGroupGizmo : GizmoDrawer
{
    //public List<PickupSpawnGizmo> children = new List<PickupSpawnGizmo>();
    private bool drawLines = false;

    private void Start()
    {
        GizmoDisplayedName = "PickupSpawnGroup";
    }

#if EDITOR
    private void OnDrawGizmos()
    {
        DrawBoxGizmo(GizmoSize, GizmoColor);
        //Gizmos.DrawIcon(Vector3.zero, GizmoDisplayedName);
        Handles.Label(transform.position, GizmoDisplayedName);

        if (drawLines)
        {
            foreach (IGizmoDrawer child in children)
            {
                Gizmos.DrawLine(((PickupSpawnGizmo)child).transform.position, transform.position);
            }
        }
    }
#endif

    public override void HandleChildrenAlfaChange(AlfaState alfaState)
    {
        foreach (PickupSpawnGizmo child in children)
        {
            child.HandleObjectAlfaChange(alfaState);
        }
    }

    public override void HandleObjectAlfaChange(AlfaState alfaState)
    {
        if (alfaState == AlfaState.selected)
        {
            drawLines = true;
            CurrentAlfa = SelectedObjectAlfa;
        }
        else
        {
            drawLines = false;
            CurrentAlfa = DefaultObjectsAlfa;
        }
    }
}
