﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GizmosDrawer;

public class ObstacleSpawnGizmo : GizmoDrawer
{
    public PlayerGroupGizmo parent;

    private void OnDrawGizmos()
    {
        DrawGizmos();
    }

    public override void HandleChildrenAlfaChange(AlfaState alfaState)
    {
        // Those guys are infertile, good for them [*]
    }

    public override void HandleObjectAlfaChange(AlfaState alfaState)
    {
        CurrentAlfa = (alfaState == AlfaState.selected) ? SelectedObjectAlfa : DefaultObjectsAlfa;
    }
}
