﻿#if UNITY_EDITOR

// Comment line below to test how it works outside of editor. 
#define EDITOR

#endif 

namespace GizmosDrawer
{
    using UnityEngine;
    using UnityEditor;
    using System.Collections.Generic;

    public class GizmoDrawerController : MonoBehaviour
    {
#if EDITOR
        public static List<IGizmoDrawer> alfableObjects = new List<IGizmoDrawer>();

        public IGizmoDrawer lastSelectedBehaviour;

        public IGizmoDrawer selectedBehaviour;

        private bool _isEnabled = false;
        public bool IsEnabled
        {
            get { return _isEnabled; }
            set
            {
                if (!value)
                {
                    UnselectAllBehaviours();
                }

                _isEnabled = value;
            }
        }

        private static GizmoDrawerSettingsSO _settingsSO;
        private static GizmoDrawerSettingsSO SettingsSO
        {
            get
            {
                if(_settingsSO == null)
                {
                    var cachedAssets = Resources.LoadAll<GizmoDrawerSettingsSO>(Extensions.scriptableObjectsResourcePath);
                    if (cachedAssets != null)
                    {
                        _settingsSO = cachedAssets[0];
                    }
                    else
                    {
                        Debug.Log($"{Extensions.scriptableObjectsResourcePath} doesnt contains GizmoDrawerSettings scriptable object asset.");
                        return null;
                    }
                }

                return _settingsSO;
            }
        }
        private static Dictionary<GizmoType, Mesh> _customMeshContainer;
        public static Dictionary<GizmoType, Mesh> CustomMeshContainer
        {
            get
            {
                if(_customMeshContainer == null)
                {
                    _customMeshContainer = new Dictionary<GizmoType, Mesh>();

                    if (SettingsSO != null)
                    {
                        for (int i = 0; i < SettingsSO.gizmoTypes.Count; i++)
                        {
                            // A little messy casting GizmoTypeCustom to int, and casting received int to GizmoType.
                            _customMeshContainer.Add((GizmoType)((int)SettingsSO.gizmoTypes[i].gizmoType), SettingsSO.gizmoTypes[i].mesh);
                        }
                    }
                }

                return _customMeshContainer;
            }
        }

        private void Start()
        {
            UnselectAllBehaviours();
        }

        private void OnDrawGizmos()
        {
            CheckSelectionAndUpdateBehaviours();
        }

        public void CheckSelectionAndUpdateBehaviours()
        {
            if (Selection.activeTransform != null)
            {
                selectedBehaviour = Selection.activeTransform.GetComponent<IGizmoDrawer>();
                if (selectedBehaviour != null)
                {
                    if (selectedBehaviour != lastSelectedBehaviour)
                    {
                        lastSelectedBehaviour?.HandleAlfaChanged(AlfaState.unselected);
                        selectedBehaviour.HandleAlfaChanged(AlfaState.selected);
                        lastSelectedBehaviour = selectedBehaviour;
                    }
                }
                else
                {
                    selectedBehaviour = Selection.activeTransform.GetComponentInParent<IGizmoDrawer>();
                    if (selectedBehaviour != null)
                    {
                        if (selectedBehaviour != lastSelectedBehaviour)
                        {
                            lastSelectedBehaviour?.HandleAlfaChanged(AlfaState.unselected);
                            selectedBehaviour.HandleAlfaChanged(AlfaState.selected);
                            lastSelectedBehaviour = selectedBehaviour;
                        }
                    }
                    else
                    {
                        lastSelectedBehaviour?.HandleAlfaChanged(AlfaState.unselected);
                        lastSelectedBehaviour = null;
                    }
                }
            }
            else
            {
                if (lastSelectedBehaviour != null)
                    lastSelectedBehaviour?.HandleAlfaChanged(AlfaState.unselected);
                lastSelectedBehaviour = null;
            }
        }

        [ContextMenu("Unselect all")]
        public void UnselectAllBehaviours()
        {
            int count = alfableObjects.Count;
            for (int i = 0; i < count; i++)
            {
                alfableObjects[i].HandleAlfaChanged(AlfaState.unselected);
            }
        }

        public static void RegisterElement(IGizmoDrawer element)
        {
            alfableObjects.Add(element);
        }

        public static void UnregisterElement(IGizmoDrawer element)
        {
            alfableObjects.Remove(element);
        }
#endif
    }
}