﻿namespace GizmosDrawer
{
    public interface IGizmoDrawer
    {
        float DefaultObjectsAlfa { get; set; }

        float SelectedObjectAlfa { get; set; }

        void Register();

        void Unregister();

        void HandleAlfaChanged(AlfaState alfaState);

        void HandleObjectAlfaChange(AlfaState alfaState);

        void HandleChildrenAlfaChange(AlfaState alfaState);
    }
}