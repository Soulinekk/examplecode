﻿#if UNITY_EDITOR

// Comment line below to test how it works outside of editor. 
#define EDITOR

#endif 

namespace GizmosDrawer
{
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class GizmoDrawer : MonoBehaviour, IGizmoDrawer
    {
#if EDITOR
        public List<IGizmoDrawer> children;
        [SerializeField]
        protected GizmoType _gizmoType = GizmoType.Box;
        protected GizmoType GizmoTypeProperty
        {
            get => _gizmoType;
            set => _gizmoType = value;
        }

        [SerializeField]
        protected virtual string GizmoDisplayedName
        {
            get; set;
        }

        [SerializeField]
        private Color _gizmoColor = Color.white;
        public virtual Color GizmoColor
        {
            get => _gizmoColor; 
            set => _gizmoColor = new Color(value.r, value.g, value.b, CurrentAlfa);
        }

        private float _currentAlfa = 0.1f;
        protected virtual float CurrentAlfa
        {
            get => _currentAlfa;
            set
            {
                _currentAlfa = value;
                GizmoColor = GizmoColor;
            }
        }

        protected float _defaultObjectsAlfa = 0.1f;
        /// <summary>
        /// Value between 0-1.
        /// </summary>
        public virtual float DefaultObjectsAlfa
        {
            get { return _defaultObjectsAlfa; }
            set
            {
                _defaultObjectsAlfa = Mathf.Clamp01(value);
            }
        }

        protected float _selectedObjectAlfa = 0.6f;
        /// <summary>
        /// Value between 0-1.
        /// </summary>
        public virtual float SelectedObjectAlfa
        {
            get { return _selectedObjectAlfa; }
            set
            {
                _selectedObjectAlfa = Mathf.Clamp01(value);
            }
        }

        [SerializeField]
        private Vector3 _gizmoSize = Vector3.one;
        protected virtual Vector3 GizmoSize
        {
            get => _gizmoSize; set => _gizmoSize = value;
        }

        [SerializeField]
        private Vector3 _gizmoOffset = Vector3.zero;
        protected virtual Vector3 GizmoOffset
        {
            get => _gizmoOffset;
            set => _gizmoOffset = value;
        }

        [SerializeField]
        private Mesh _customMesh;
        protected Mesh CustomMesh
        {
            get
            {
                if (_customMesh == null && GizmoDrawerController.CustomMeshContainer != null)
                {
                    GizmoDrawerController.CustomMeshContainer.TryGetValue(GizmoTypeProperty, out _customMesh);
                    //_customMesh = GizmoDrawerController.CustomMeshContainer[GizmoTypeProperty];
                }

                return _customMesh;
            }
        }

        private void Start()
        {
            Register();
        }

        public virtual void Register()
        {
            GizmoDrawerController.RegisterElement(this);
        }

        public virtual void Unregister()
        {
            GizmoDrawerController.UnregisterElement(this);
            GizmoDrawerController parent = transform.root.GetComponent<GizmoDrawerController>();
            if (parent != null)
            {
                parent.lastSelectedBehaviour = null;
            }
        }

        public virtual void SetSettings(GizmoObjectData settings)
        {
            GizmoDisplayedName = settings.name;
            GizmoSize = settings.gizmoSize;
            GizmoColor = settings.gizmoColor;
            GizmoTypeProperty = settings.gizmoType;
        }

        public virtual void HandleAlfaChanged(AlfaState alfaState)
        {
            HandleObjectAlfaChange(alfaState);
            HandleChildrenAlfaChange(alfaState);
        }

        public abstract void HandleObjectAlfaChange(AlfaState alfaState);

        public abstract void HandleChildrenAlfaChange(AlfaState alfaState);

        protected void DrawGizmos()
        {
            if (GizmoTypeProperty == GizmoType.Box)
            {
                DrawBoxGizmo(GizmoSize, GizmoColor);
            }
            else if (GizmoTypeProperty == GizmoType.Sphere)
            {
                // Yee, yee, i know, didnt imprement size for sphere, maybe letter :D
                DrawSphereGizmo(1, GizmoColor);
            }
            else
            {
                DrawMeshGizmo(CustomMesh, GizmoSize, GizmoColor);
            }
        }

        protected void DrawBoxGizmo(Vector3 size, Color color)
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = color;

            Gizmos.DrawWireCube(GizmoOffset, size);
            // Drawing clickable gizmo
            if (!Event.current.capsLock)
            {
                Gizmos.DrawCube(GizmoOffset, size);
            }
            // Drawing short line in front of gizmos. Helps setting correct rotation.
            Gizmos.color = Color.red;
            Gizmos.matrix = Matrix4x4.identity;
            Vector3 from = transform.TransformPoint(GizmoOffset);
            from += transform.forward * (GizmoOffset.z + (transform.localScale.z / 2));
            Gizmos.DrawRay(from, transform.TransformDirection(Vector3.forward) * 2f);
        }

        protected void DrawSphereGizmo(float radius, Color color)
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = color;

            Gizmos.DrawWireSphere(GizmoOffset, radius);
            if (!Event.current.capsLock)
            {
                Gizmos.DrawSphere(GizmoOffset, radius);
            }

            // Drawing short line in front of gizmos. Helps setting correct rotation.
            Gizmos.color = Color.red;
            Gizmos.matrix = Matrix4x4.identity;
            Vector3 from = transform.TransformPoint(GizmoOffset);
            from += transform.forward * (GizmoOffset.z + (transform.localScale.z / 2));
            Gizmos.DrawRay(from, transform.TransformDirection(Vector3.forward) * 2f);
        }

        protected void DrawMeshGizmo(Mesh mesh, Vector3 size, Color color)
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = color;

            if (mesh == null)
            {
                Debug.Log("Custom mesh is null, yet you are trying to draw gizmo with it");
                return;
            }

            Gizmos.DrawWireMesh(mesh, GizmoOffset, Quaternion.identity, size);
            if (!Event.current.capsLock)
            {
                Gizmos.DrawMesh(mesh, GizmoOffset, Quaternion.identity, size);
            }
        }

        private void OnDestroy()
        {
            Unregister();
        }
#endif
    }
}