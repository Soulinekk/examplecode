﻿using UnityEngine;
using OverlayWindow;

[ExecuteInEditMode]
public class InitializeExample : MonoBehaviour
{
    void Start()
    {
        if (!OverlayWindowDrawer.IsEnabled)
        {
            OverlayWindowDrawer.IsEnabled = true;
            OverlayWindowDrawer.InitializeGizmoDrawer();
            OverlayWindowDrawer.ExampleSpawner.Spawn();
        }
    }
}
